const mysql = require("mysql");

const con = mysql.createConnection({
    host:"localhost",
    user: "root",
    password:"",
    port: "3306",
    database:"exam"
});

con.connect((err)=>{
    if(err) throw err;
    console.log("Connected");
})

let users = {};

users.all = ()=>{
    return new Promise((resolve,reject)=>{
        con.query("SELECT username,id FROM authentication;",((err,result)=>{
            if(err){
                return reject(err);
            }
            return resolve (result);
        }))
    })
}

users.register = (username, password, email )=> {
    return new Promise((resolve,reject)=>{
        con.query(`INSERT INTO authentication (username, password, email) VALUES ( "${username}", "${password}",  "${email}" ) ;`,((err,result)=>{
            if(err){
                return reject(err);
            }
            return resolve (result);
        }))
    })
}

users.login = (username) => {
    return new Promise((resolve,reject)=>{
        con.query(`SELECT username,password,id FROM authentication WHERE username = '${username}' ;`,((err,result)=>{
            if(err){
                return reject(err);
            }
            return resolve (result);
        }))
    })
}

module.exports = users;
