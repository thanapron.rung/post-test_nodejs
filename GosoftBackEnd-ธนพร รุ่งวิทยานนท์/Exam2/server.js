const express = require("express");
const db = require("./TestDB");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const auth = require("./auth");
const secret = "SecretPassword";
const cookieParser = require("cookie-parser");

const app = express();

app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.set ( "view engine", "ejs" );
app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

app.get("/",(req,res)=>{
    res.render("home")
});

app.post("/register", async (req, res)=>{
    try {
        const {username, password, confirmPassword, email, confirmEmail} = req.body;
        if( username == null || password == null || email == null || confirmEmail == null || confirmPassword == null ) {
            return res.status(201).send({
                message:"data's missing"
            });
        }

        if(password == confirmPassword && email == confirmEmail) {

            const checkUsername = await db.all();
            const found = JSON.parse(JSON.stringify(checkUsername)).find(e=> e.username == username);
            if(found){
                return res.status(200).send({
                    message: "user is exist!"
                })
            }
    
            const salt = bcrypt.genSaltSync(10);
            const hash = bcrypt.hashSync(password, salt);
            await db.register(username,hash,email);
            return res.status(200).redirect('/register_completed');
        }
        
    } catch (error) {
        console.log(error);
        res.status(500);
    }
});

app.get("/register_completed", async (req, res) => {
    const info = await db.all();
    // const found = JSON.parse(JSON.stringify(info))
    res.render("registerComplete",{info:info})
})

app.get("/login", async (req, res) => {
    res.render("login");
})

app.post("/login_completed", async (req, res,next) => {
    const {username, password} = req.body;
    try {
        if( username == null || password == null) {
            return res.status(201).send({
                message:"data's missing"
            });
        }
        const data = await db.login(username);
        const dataarr = JSON.parse(JSON.stringify(data));
        const uname = dataarr[0].username;
        const id = dataarr[0].id

        if(dataarr.length != 0){
            bcrypt.compareSync(password, dataarr[0].password);
            const token = jwt.sign({username:uname, id:id},secret);
            res.cookie("access-token", token, {
                maxAge: 60 * 60 * 24 * 30 * 1000,
                httpOnly: true,
              });
        
            res.status(200).redirect(`/profile`);
        } else {
            return res.status(200).render("loginfail");
        }
    } catch (error) {
        console.log(error);
        res.status(500);
    }
})

app.get("/profile",auth,(req, res)=>{
    const accessToken = req.cookies["access-token"];
    jwt.verify(accessToken,secret,(err,result)=>{
        if(err){
            console.log(err);
            res.status(403).redirect("/login");
        } else {
            res.status(200).render("profile",{info:[result]});
        }
    })
})

app.get("/logout",(req, res)=>{
    return res.clearCookie('access-token').render("logout");
})



app.listen(7555,()=>{
    console.log("Server is running")
});


