const jwt = require("jsonwebtoken");
const secret = "SecretPassword";

module.exports = (req, res, next) => {
    try {
        const accessToken = req.cookies["access-token"];
        console.log(accessToken )
        if (!accessToken ) return res.status(403).redirect("/login");
        const decoded = jwt.verify(accessToken , secret);
        console.log(decoded)
        req.user = decoded;
        next();
    } catch (error) {
        console.log(error)
        res.status(400).redirect("/login")
    }
};
